const staticRecordingApp = "dev-recording-app"
const assets = [
    "/",
    "/index.html",
    "/sass/styles.css",
    "/js/functions.js",
]

self.addEventListener("install", installEvent => {
    installEvent.waitUntil(
        caches.open(staticRecordingApp).then(cache => {
            return cache.addAll(assets)
        })
    )
})

self.addEventListener("fetch", fetchEvent => {
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(res => {
            return res || fetch(fetchEvent.request)
        })
    )
})