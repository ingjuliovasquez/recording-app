// TODO: Define variables
// * Button start recording
const btnStart = document.querySelector('#btn-start');
// * Button stop recording
const btnStop = document.querySelector('#btn-stop');
// * Bars background
const bars = document.querySelector('.bars');
// * Button list view
const listRecording = document.querySelector('.btn-list');
// * Verify browser support
const browserSupport = (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) ? true : false;
// * Media recorder
let media = null;
// * Stream media
let mediaStream = null;
// * Audio files
let audioBlobs = [];
// * Audio list
let recordingList = [];
// * Interval counter
let interval = null;
// * View list
let listView = false;
// * Difine if is recording audio
let isRecording = false;

// TODO: Define functions
// * Show alert message if the browser not supported mediaDevices API
window.onload = () => { if (!browserSupport) alert("Navegador no compatible."); };

// * Register serviceWorker
// if ("serviceWorker" in navigator) {
//     window.addEventListener("load", function () {
//         navigator.serviceWorker
//             .register("./serviceWorker.js")
//             .then(res => console.log("service worker registered"))
//             .catch(err => console.log("service worker not registered", err))
//     })
// }

// * Start audio recording
function startAudioRecording() {
    if (browserSupport) {
        isRecording = true;

        // * Hide button to start recording
        btnStart.style.display = "none";

        // * Show button to stop recording
        btnStop.style.display = "inline";

        navigator.mediaDevices.getUserMedia({ audio: true })
            .then((stream) => {
                // * Set stream
                mediaStream = stream;

                // * Set stream audio
                media = new MediaRecorder(stream);

                // * Clear previously audio fragments/blobs
                audioBlobs = [];

                // * Set audio fragments/blobs
                media.addEventListener("dataavailable", event => {
                    audioBlobs.push(event.data);
                });

                // * Add animate clas to bars audio
                bars.classList.add('bars-animate');

                // * Start time counter recording
                setTimeRecording(true);

                // * Start recording
                media.start();
            })
            .catch((err) => {
                console.log(err);
            });
    }
}

// * Stop audio recording
function stopAudioRecording() {
    if (browserSupport) {
        isRecording = false;

        // * Show button to start recording
        btnStart.style.display = "inline";

        // * Hide button to stop recording
        btnStop.style.display = "none";

        // * Set audio type
        let mimeType = media.mimeType;

        // * Add listener on stop recording
        media.addEventListener("stop", () => {
            // * Make unique blob audio
            let audioBlob = new Blob(audioBlobs, { type: mimeType });

            // * Push to array, new audio blob
            recordingList.push(audioBlob);
        });

        // * Remove animate clas to bars audio
        bars.classList.remove('bars-animate');

        // * Stop media
        media.stop();

        // * Stop stream
        mediaStream.getTracks().forEach(track => track.stop());

        // * Reset properties
        resetProperties();

        // * Stop time counter recording
        setTimeRecording(false);
    }
}

// * Delete audio recording
function deleteAudioRecording(index) {
    // * Remove element to array
    recordingList.splice(index, 1);

    // * Relaod list audio recordings
    loadRecordings();
}

// * Reset properties
function resetProperties() {
    media = null;
    mediaStream = null;
}

// * Set time recording
function setTimeRecording(isRecord) {
    // * Define variables
    let hours = 0;
    let minutes = 0;
    let seconds = 0;

    // * Define elements
    const hoursElem = document.querySelector('.hours');
    const minutesElem = document.querySelector('.minutes');
    const secondsElem = document.querySelector('.seconds');

    if (isRecord) { // * Start counter
        interval = setInterval(() => {
            seconds++;
            secondsElem.innerHTML = seconds > 9 ? seconds : `0${seconds}`;

            if (seconds == 60) {
                seconds = 0;
                minutes++

                minutesElem.innerHTML = minutes > 9 ? minutes : `0${minutes} : `;
            }
            if (minutes == 60) {
                minutes = 0;
                hours++

                hoursElem.innerHTML = hours > 9 ? hours : `0${hours} : `;
            }
        }, 1000);
    } else { // * Stop counter
        clearInterval(interval);
        secondsElem.innerHTML = '00';
        minutesElem.innerHTML = '00 : ';
        hoursElem.innerHTML = '00 : ';
    }
}

// * Swith view
function changeView() {
    const container = document.querySelector(".main");
    const index = document.querySelector("#recording-index");
    const list = document.querySelector("#recording-list");

    if (listView) {
        listView = false;
        index.style.display = "block";
        list.style.display = "none";
        container.style.display = "flex";
    } else {
        listView = true;
        index.style.display = "none"
        list.style.display = "block";
        container.style.display = "block";

        // Validate if the app is recording and stop to recording when switch view to list audio recording 
        if (isRecording) stopAudioRecording();

        loadRecordings();
    }
}

// * Load recording list
async function loadRecordings() {
    // * Content audio recording list
    const contentList = document.querySelector("#recording-list");

    // * Reset div audio recording list
    contentList.innerHTML = "";

    // * Make recordings elements promises
    const audioElements = recordingList.map((blob, index) => {
        return new Promise((resolve) => {
            const reader = new FileReader();

            reader.readAsDataURL(blob);

            reader.onload = (e) => {
                // * Get type audio blob
                let BlobType = blob.type.includes(";") ? blob.type.substr(0, blob.type.indexOf(';')) : blob.type;

                const element = `
                    <div class='audio-file'>
                        <div class="header-file">
                            <label>Record #${index + 1}</label>
                            <a href="#" class="delete-option" index="${index}">Delete</a>
                        </div>
                        <audio id="audio-player" controls="controls" src="${e.target.result}" type="${BlobType}">
                    </div>
                `;

                contentList.innerHTML += element;

                resolve(contentList);
            }
        })
    });

    if (await Promise.all(audioElements)) { // * Resolve all promises
        // * Get all delete options elements
        const deleteOptions = document.querySelectorAll(".delete-option");

        // ** Add click event to all elements 
        deleteOptions.forEach((element) => {
            element.onclick = function (e) {
                // * Get index audio recording
                const recordingIndex = e.target.getAttribute("index");

                // * Delete audio recording
                deleteAudioRecording(recordingIndex);
            }
        });
    }
}

// TODO: Define events
// * Button event start audio recording
btnStart.onclick = (e) => {
    e.preventDefault();
    startAudioRecording();
};

// * Button event stop audio recording
btnStop.onclick = (e) => {
    e.preventDefault();
    stopAudioRecording();
};

// * Button event change view index to list recording
listRecording.onclick = (e) => {
    e.preventDefault();
    changeView();
}